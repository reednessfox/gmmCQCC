# ====================================================================================
# LOAD DATA FROM TXT-FILE
# ====================================================================================

import numpy as np

def loadData(database):
    if database == 'emovo':
        datasets = [ '/data.txt', '/TimeVec.txt', '/FreqVec.txt', '/vadIDX.txt']
        data = []
        rab = np.zeros(shape=(1,63))
        dis = np.zeros(shape=(1,63))
        pau = np.zeros(shape=(1,63))
        gio = np.zeros(shape=(1,63))
        neu = np.zeros(shape=(1,63))
        tri = np.zeros(shape=(1,63))
        sor = np.zeros(shape=(1,63))

        for i in range(1, 558):
            filename1 = 'results/cqcc/features/' + str(i) + datasets[0]
            features = np.genfromtxt(filename1, delimiter=',')

            if int(features[0][60]) == 0: rab = np.vstack((rab, features))
            elif int(features[0][60]) == 1: dis = np.vstack((dis, features))
            elif int(features[0][60]) == 2: pau = np.vstack((pau, features))
            elif int(features[0][60]) == 3: gio = np.vstack((gio, features))
            elif int(features[0][60]) == 4: neu = np.vstack((neu, features))
            elif int(features[0][60]) == 5: tri = np.vstack((tri, features))
            elif int(features[0][60]) == 6: sor = np.vstack((sor, features))

        print(np.shape(rab))

        data.append(rab)
        data.append(dis)
        data.append(pau)
        data.append(gio)
        data.append(neu)
        data.append(tri)
        data.append(sor)

    else: print('wrong name database')

    for i in range(len(data)):
        print(data[i].shape)

    return data

# ====================================================================================
# DIVISION ON TEST AND TRAIN
# ====================================================================================
#  test (0.3) and train (0.7)

from sklearn.model_selection import train_test_split

X_test = []
y_test = []
X_train = []
y_train = []

def division_test_and_train(emo_matrix):
    size_test = int(emo_matrix.shape[0]*0.3)
    size_train = int(emo_matrix.shape[0]*0.7)

    test_indxs = np.random.randint(emo_matrix.shape[0], size=size_test)
    train_indxs = np.random.randint(emo_matrix.shape[0], size=size_train)
    _test = []
    _train = []

    for i in range(test_indxs.shape[0]):
        indx = test_indxs[i]
        _test.append(emo_matrix[indx])

    for i in range(train_indxs.shape[0]):
        indx = train_indxs[i]
        _train.append(emo_matrix[indx])

    _test = np.array(_test)
    _train = np.array(_train)

    X_train = _train[:, 0:60]
    y_train = _train[:, 60].reshape(-1,1)

    X_test = _test[:, 0:60]
    y_test = _test[:, 60].reshape(-1,1)
      
    return X_train, y_train, X_test, y_test

def stack_test(X_test, y_test):
    tmp1 = X_test[0]
    tmp2 = X_test[1]
    X_stck = np.vstack((tmp1, tmp2))

    for i in range(2, len(X_test)):
        tmp1 = X_test[i]
        X_stck = np.vstack((X_stck, tmp1))
    tmp1 = y_test[0]
    tmp2 = y_test[1]
    y_stck = np.vstack((tmp1, tmp2))

    for i in range(2, len(X_test)):
        tmp1 = y_test[i]
        y_stck = np.vstack((y_stck, tmp1))

    return X_stck, y_stck

#from google.colab import drive
#drive.mount('/content/gdrive/')

database = 'emovo'
data = loadData(database)

for i in range(len(data)):
    _X_train, _y_train, _X_test, _y_test = division_test_and_train(data[i])

    X_test.append(_X_test)
    y_test.append(_y_test)
    X_train.append(_X_train)
    y_train.append(_y_train)

X_test, y_test = stack_test(X_test, y_test)

for i in range(len(X_train)):
    print('X_train shape:', X_train[i].shape, 'y_train shape:', y_train[i].shape)
print('X test:', X_test.shape, 'y test:', y_test.shape)

import warnings
warnings.filterwarnings('ignore')

# ====================================================================================
# TRAIN GAUSSIAN MUXTURE MODEL
# ====================================================================================
#   components: 100/500/1000
#   covariance_type: diagonal
#   models: 7
#   max_iter: 1/5/10/20/50/100 (default: 100)
# ====================================================================================
#   components: 100/500/1000
#   covariance_type: diagonal
#   models: 7
#   max_iter: 1/5/10/20/50/100 (default: 100)
# ======================================================
#  confusion matrix

from sklearn import metrics
import matplotlib.pyplot as plt
import seaborn as sns

itr = [ 1, 5, 10, 20, 50, 100 ]
comp = [ 100, 500, 1000 ]

title_list = []
filenames = []
for i in range(len(itr)):
    for j in range(len(comp)):
        title = u'GMM: iter ' + str(itr[i]) + ', components ' + str(comp[j])
        title_list.append(title)

for i in range(len(title_list)):
    filename = 'results/mfcc/confusionMatrix/' + title_list[i] + '.png'
    filenames.append(filename)

def calculate_results(scrs, filenames, ttl):
    frst = scrs[0].reshape(-1,1)
    scnd = scrs[1].reshape(-1,1)
    a = np.hstack((frst, scnd))

    for i in range(2, len(scrs)):
        frst = scrs[i].reshape(-1,1)
        a = np.hstack((a, frst))
    
    Pred_cnfmtrx = []

    for i in range(a.shape[0]):
        b = np.argmax(a[i])
        Pred_cnfmtrx.append(b)

    Pred_cnfmtrx = np.array(Pred_cnfmtrx)
    print(Pred_cnfmtrx, Pred_cnfmtrx.shape)

    True_cnfmtrx = []

    for i in range(y_test.shape[0]):
        tmp = y_test[i].astype(np.int64)
        True_cnfmtrx.append(tmp)
    True_cnfmtrx = np.array(True_cnfmtrx).reshape(-1)
    print(True_cnfmtrx, True_cnfmtrx.shape)
    print()
    print('accuracy score:', metrics.accuracy_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx))
    print('f1 score <macro>:', metrics.f1_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx, average='macro'))
    print('f1 score <micro>:', metrics.f1_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx, average='micro'))

    cm = metrics.confusion_matrix(True_cnfmtrx, Pred_cnfmtrx)

    plt.figure(figsize=(12,8))
    plt.title(ttl)
    sns.heatmap(cm, square=True, annot=True, cbar=False)
    plt.xlabel('predicted value')
    plt.ylabel('true value')

    plt.show()

def calculate_results_percent(scrs, filenames, ttl):
    frst = scrs[0].reshape(-1,1)
    scnd = scrs[1].reshape(-1,1)
    a = np.hstack((frst, scnd))

    for i in range(2, len(scrs)):
        frst = scrs[i].reshape(-1,1)
        a = np.hstack((a, frst))

    Pred_cnfmtrx = []
    for i in range(a.shape[0]):
        b = np.argmax(a[i])
        Pred_cnfmtrx.append(b)
    Pred_cnfmtrx = np.array(Pred_cnfmtrx)
    print(Pred_cnfmtrx, Pred_cnfmtrx.shape)

    True_cnfmtrx = []

    for i in range(y_test.shape[0]):
        tmp = y_test[i].astype(np.int64)
        True_cnfmtrx.append(tmp)

    True_cnfmtrx = np.array(True_cnfmtrx).reshape(-1)
    print(True_cnfmtrx, True_cnfmtrx.shape)
    
    print()
    accuracy = metrics.accuracy_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx)
    print('accuracy score:', accuracy)
    print('f1 score <macro>:', metrics.f1_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx, average='macro'))
    print('f1 score <micro>:', metrics.f1_score(y_true=True_cnfmtrx, y_pred=Pred_cnfmtrx, average='micro'))

    cm = metrics.confusion_matrix(True_cnfmtrx, Pred_cnfmtrx)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.figure(figsize=(12,8))
    plt.title(ttl)
    sns.heatmap(cm, square=True, annot=True, cbar=False)
    plt.xlabel('predicted value')
    plt.ylabel('true value')
    plt.savefig(filename)
    plt.show()
    
    return accuracy

import warnings
warnings.filterwarnings('ignore')

# ====================================================================================
#  train gaussian mixture models
# ====================================================================================

from sklearn import mixture

models = []
scores = []
accuracies = []

count = 0

for i in range(len(itr)):
    for j in range(len(comp)):
        print('num iteration:', itr[i], '| num componets:', comp[j])
        mdls = []
        scrs = []

        for k in range(len(X_train)):
            gmm = mixture.GaussianMixture(n_components=comp[j], covariance_type='diag', max_iter=itr[i])
            mod = gmm.fit(X_train[k])
            scor = gmm.score_samples(X_test).reshape(-1,1)
            mdls.append(mod)
            scrs.append(scor)

            print(' train progress:', k+1, 'in', len(X_train))
        print()  

        calculate_results(scrs, filenames[count], title_list[count])
        accuracy = calculate_results_percent(scrs, filenames[count], title_list[count])

        print()
        print()

        models.append(mdls)
        scores.append(scrs)
        accuracies.append(accuracy)

        count += 1

XgmmScores = scores[0][0]

for i in range(len(scores[0])-1):
    XgmmScores = np.hstack((XgmmScores, scores[0][i+1]))

print(XgmmScores.shape, y_test.shape)

np.savetxt('results/cqcc/XgmmScores.csv', XgmmScores, delimiter=',')
np.savetxt('results/cqcc/ygmmScores.csv', y_test, delimiter=',')
